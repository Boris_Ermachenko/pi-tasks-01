#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

int main()
{
    int foot = 0;
    int inch = 0;
    double sm = 0;
    const int foottoinch = 12;
    const double inchtosm = 2.54;

    printf("Enter your height (ff.ii) \n");
    scanf("%d.%d", &foot, &inch);
    if (foot < 0 || inch < 0 || inch >99)
    {
        printf("Wrong value \n");
        return 1;
    }
    else
    {
        sm = ((foot*foottoinch)*inchtosm)+(inch*inchtosm);
        printf("Your height is %f sm \n",sm);
        return 0;
    }
}